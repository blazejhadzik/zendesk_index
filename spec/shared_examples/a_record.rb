RSpec.shared_examples 'a record' do
  context 'whitelisted attrs' do
    it 'defines its ATTR_LIST' do
      expect(described_class::ATTR_LIST).not_to be_nil
    end
  end

  context 'when using non-whitelisted param' do
    subject { described_class.new(unpermitted: true) }

    describe '.initialize' do
      it 'ignores param' do
        expect(subject.instance_variable_get(:@unpermitted)).to be_nil
      end
    end

    describe '#[]' do
      it 'raises error' do
        expect do
          subject[:unpermitted]
        end.to raise_error(SearchTermNotAllowedError)
      end
    end
  end

  context 'when using whitelisted param' do
    let(:value) { 123 }
    let(:attr_name) { described_class::ATTR_LIST.sample }
    subject { described_class.new(attr_name => value) }

    describe '.initialize' do
      it 'sets instance variable' do
        expect(subject.instance_variable_get("@#{attr_name}")).not_to be_nil
      end
    end

    describe '#[]' do
      it 'returns value for given key' do
        expect(subject[attr_name]).to eq value
      end
    end
  end
end
