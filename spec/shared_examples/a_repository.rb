RSpec.shared_examples 'a repository' do
  before do
    allow_any_instance_of(described_class).to receive(:source_path) { data_path }
    allow_any_instance_of(described_class).to receive(:connect!).and_call_original
    allow(JSON).to receive(:parse).and_call_original
  end

  subject { described_class.new }

  context 'acts_as_repository' do
    it 'implements base_klass' do
      expect { subject.base_klass }.not_to raise_error
    end

    it 'implements source_path' do
      expect { subject.source_path }.not_to raise_error
    end

    it 'implements symbolize_names' do
      expect { subject.symbolize_names }.not_to raise_error
    end
  end

  describe '.acts_as_repository?' do
    it 'returns true' do
      expect(described_class.acts_as_repository?).to eq true
    end
  end

  describe '.initialize' do
    let(:users_file) { File.read(data_path) }

    it 'connects to data source on initialize' do
      expect(subject).to have_received(:connect!)
    end

    it 'loads json and parses with params' do
      subject
      expect(JSON).to have_received(:parse).with(
        users_file,
        symbolize_names: true
      )
    end

    context 'when file does not exist' do
      let(:data_path) { '/non/existing/path' }

      it 'loads json and parses with params' do
        expect { subject }.to raise_error(MissingDataSourceError)
      end
    end
  end

  describe '#where' do
    let(:cluster) { instance_double(Cluster::Table) }
    let(:key) { '_id' }
    let(:value) { '123' }

    before do
      allow(Cluster::Table).to receive(:new) { cluster }
      allow(cluster).to receive(:index)
      allow(cluster).to receive(:search)

      subject.where(key: key, value: value)
    end

    it 'builds its cluster' do
      expect(Cluster::Table).to have_received(:new).with(described_class)
    end

    it 'delegates index creation to its cluster' do
      expect(cluster).to have_received(:index).with(key)
    end

    it 'delegates query to its cluster' do
      expect(cluster).to have_received(:search).with(value)
    end
  end

  describe '#all' do
    it 'returns parsed collection' do
      expect(subject.all).to all(be_kind_of(base_klass))
    end
  end

  describe '#attributes' do
    it 'returns its baseclass attrs' do
      expect(subject.attributes).to eq base_klass::ATTR_LIST
    end
  end

  describe '#base_klass' do
    it 'returns its base_klass' do
      expect(subject.base_klass).to eq base_klass
    end
  end

  describe '#source_path' do
    it 'returns its source_path' do
      expect(subject.source_path).to eq data_path
    end
  end

  describe '#symbolize_names' do
    it 'returns its symbolize_names' do
      expect(subject.symbolize_names).to eq true
    end
  end
end
