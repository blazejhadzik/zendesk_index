require 'spec_helper'

RSpec.describe ZendeskIndex::Search do
  let(:data_path) do
    File.expand_path('../../fixtures/users.json', __FILE__)
  end

  before do
    allow_any_instance_of(UserRepository).to receive(:source_path) { data_path }
    allow_any_instance_of(TicketRepository).to receive(:source_path) { data_path }
    allow_any_instance_of(OrganizationRepository).to receive(:source_path) { data_path }
  end

  subject { described_class.new }

  describe '#help' do
    context 'given invalid repo name' do
      let(:query_params) do
        { repo_choice: 'invalid' }
      end

      it 'raises UndefinedDataSourceError error' do
        expect do
          subject.help(query_params).to raise_error(UndefinedDataSourceError)
        end
      end
    end

    context 'given valid repo name' do
      let(:repository) { UserRepository }

      let(:help_params) do
        { repo_choice: 'Users' }
      end

      before do
        allow(repository).to receive(:new).with(any_args).and_call_original
      end

      it 'returns ATTR_LIST for its base klass' do
        expect(subject.help(help_params)).to eq(User::ATTR_LIST)
      end
    end

    describe '#query' do
      context 'given invalid repo name' do
        let(:query_params) do
          { repo_choice: 'invalid' }
        end

        it 'raises UndefinedDataSourceError error' do
          expect do
            subject.query(query_params).to raise_error(UndefinedDataSourceError)
          end
        end
      end

      context 'given valid repo name' do
        let(:results) { double }
        let(:repository) { double }
        let(:search_attr) { '_id' }
        let(:search_phrase) { '33' }

        let(:query_params) do
          {
            repo_choice: 'Users',
            search_attr: search_attr,
            search_phrase: search_phrase
          }
        end

        before do
          allow(UserRepository).to receive(:new).with(any_args) { repository }
          allow(repository).to receive(:where).with(any_args) { results }

          subject.query(query_params)
        end

        context 'given phrase is nil' do
          let(:query_params) do
            {
              repo_choice: 'Users',
              search_attr: nil,
              search_phrase: nil
            }
          end
          it 'delegates query to db with stringified values' do
            expect(repository).to have_received(:where).with(
              key: '',
              value: ''
            )
          end
        end

        it 'delegates query to db' do
          expect(repository).to have_received(:where).with(
            key: search_attr,
            value: search_phrase
          )
        end

        it 'returns query results' do
          expect(subject.query(query_params)).to eq results
        end
      end
    end
  end
end
