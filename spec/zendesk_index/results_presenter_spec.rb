require 'spec_helper'

RSpec.describe ResultsPresenter do
  let(:example_class) { User }
  let(:collection_item) { example_class.new(_id: 123) }
  let(:collection) { [collection_item] }

  describe '.render_list' do
    let(:item_1) { double }
    let(:item_2) { double }
    let(:item_list) { [item_1, item_2] }

    subject { described_class.render_list(item_list) }

    before do
      allow(STDOUT).to receive(:puts)
      allow(item_1).to receive(:to_s)
      allow(item_2).to receive(:to_s)

      subject
    end

    it 'renders item_list items to STDOUT' do
      expect(STDOUT).to have_received(:puts)
    end

    it 'calls to_s on each item_list item' do
      expect(item_list).to all(have_received(:to_s))
    end
  end

  describe '.render_collection' do
    subject { described_class.render_collection(collection) }

    before do
      allow(STDOUT).to receive(:puts)
      allow(collection_item).to receive(:to_h).and_call_original
      subject
    end

    it 'renders collection and its N items to STDOUT. Appends extra line.' do
      expect(STDOUT).to have_received(:puts).exactly(
        example_class::ATTR_LIST.count + 1
      ).times
    end

    it 'calls to_h on each collection_item' do
      expect(collection).to all(have_received(:to_h))
    end
  end
end
