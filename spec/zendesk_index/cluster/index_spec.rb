require 'spec_helper'

RSpec.describe Cluster::Index do
  let(:index_key) { 'foo' }
  subject { described_class.new(index_key) }

  describe '.initialize' do
    context 'given no key' do
      it 'raises SearchTermNotAllowedError error' do
        expect do
          described_class.new
        end.to raise_error(SearchTermNotAllowedError)
      end
    end

    context 'given index key' do
      it 'assigns symbolised key' do
        expect(
          subject.instance_variable_get(:@indexable)
        ).to eq index_key.to_sym
      end

      it 'initiases index hash' do
        expect(subject.instance_variable_get(:@dictionary)).to eq({})
      end
    end
  end

  describe '#index' do
    context 'when obj does not have _id property' do
      let(:indexable) { {} }
      it 'raises SearchTermNotAllowedError error' do
        expect do
          subject.index(indexable)
        end.to raise_error(IdAttributeMissingError)
      end
    end

    context 'when obj does have _id property' do
      before { subject.index(indexable) }

      context 'when obj to index is an Array' do
        let(:id) { 3 }
        let(:item_1) { 'foo' }
        let(:item_2) { 'bar' }
        let(:item_3) { 'baz' }
        let(:item_4) { 'baz' }

        let(:items) { [item_1, item_2, item_3, item_4] }

        let(:indexable) do
          OpenStruct.new(
            :_id => id,
            index_key => items
          )
        end

        it 'indexes all uniq items' do
          expect(subject.dictionary.length).to eq items.uniq.count
        end

        it 'aggregates same items under one hash' do
          expect(subject.dictionary[item_3.hash].length).to eq 2
        end
      end

      context 'when obj to index is a String' do
        let(:id) { 3 }
        let(:item) { 'foo' }

        let(:indexable) do
          OpenStruct.new(
            :_id => id,
            index_key => item
          )
        end

        it 'indexes item using hash' do
          expect(subject.dictionary[item.hash]).to eq [id.hash]
        end
      end
    end
  end

  describe '#[]' do
    context 'when no previously indexed matches found' do
      it 'returns []' do
        expect(subject['undefined']).to eq []
      end
    end

    context 'when previously indexed matches found by hash' do
      let(:search_phrase) { 'bar' }
      let(:id) { 3 }
      let(:indexable) do
        OpenStruct.new(
          :_id => id,
          index_key => search_phrase
        )
      end

      before { subject.index(indexable) }

      it 'returns matches' do
        expect(subject[search_phrase.hash]).to include id.hash
      end
    end
  end
end
