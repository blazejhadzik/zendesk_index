require 'spec_helper'

RSpec.describe Cluster::Table do
  let(:record) { User.new(_id: 2) }
  let(:repo_records) { [record] }

  let(:repository) { double(all: repo_records) }
  subject { described_class.new(repository) }

  describe '.initialize' do
    it 'assigns repository' do
      expect(subject.instance_variable_get(:@repository)).to eq repository
    end
  end

  describe '#index' do
    let(:search_attribute) { 'name' }
    let(:index_table) { instance_double(Cluster::Index) }

    before do
      allow(Cluster::Index).to receive(:new) { index_table }
      allow(index_table).to receive(:index).with(any_args)

      subject.index(search_attribute)
    end

    it 'creates new IndexTable object using search_attribute' do
      expect(Cluster::Index).to have_received(:new).with(search_attribute)
    end

    it 'iterates over @repository and indexes each item' do
      expect(index_table).to have_received(:index).exactly(
        repo_records.count
      ).times
    end
  end

  describe '#search' do
    let(:phrase) { 'foo' }

    context 'when @index_table undefined' do
      it 'raises NoIndexTableFoundError' do
        expect { subject.search(phrase) }.to raise_error(NoIndexTableFoundError)
      end
    end

    context 'when @index_table is defined' do
      let(:index_key) { :_id }

      before do
        allow(subject).to receive(:hashify).and_call_original
        allow(subject).to receive(:find_by_ids).and_call_original
        subject.index(index_key)

        subject.search(phrase)
      end

      it 'hashifies query phrase' do
        expect(subject).to have_received(:hashify).with(phrase)
      end

      it 'filters @index_table result against the entire DB' do
        expect(subject).to have_received(:find_by_ids)
      end

      context 'when no matches found' do
        it 'returns records matching hashified ids' do
          expect(subject.search(phrase)).to eq []
        end
      end

      context 'when matches found' do
        let(:phrase) { record[index_key].to_s }

        it 'returns records matching hashified ids' do
          expect(subject.search(phrase)).to include(record)
        end
      end
    end
  end
end
