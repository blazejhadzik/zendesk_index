require 'spec_helper'

RSpec.describe UserRepository do
  it_behaves_like 'a repository' do
    let(:base_klass) { User }
    let(:data_path) do
      File.expand_path('../../../fixtures/users.json', __FILE__)
    end
  end
end
