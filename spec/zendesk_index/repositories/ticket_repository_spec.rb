require 'spec_helper'

RSpec.describe TicketRepository do
  it_behaves_like 'a repository' do
    let(:base_klass) { Ticket }
    let(:data_path) do
      File.expand_path('../../../fixtures/tickets.json', __FILE__)
    end
  end
end
