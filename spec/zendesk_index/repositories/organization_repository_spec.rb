require 'spec_helper'

RSpec.describe OrganizationRepository do
  it_behaves_like 'a repository' do
    let(:base_klass) { Organization }
    let(:data_path) do
      File.expand_path('../../../fixtures/organizations.json', __FILE__)
    end
  end
end
