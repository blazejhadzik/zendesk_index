require 'spec_helper'

RSpec.describe do
  context 'custom errors' do
    it 'raises errors with custom messages' do
      expect do
        raise NoIndexTableFoundError
      end.to raise_error(
        NoIndexTableFoundError,
        'Index has not been created yet. Create index before making query.'
      )

      expect do
        raise SearchTermNotAllowedError
      end.to raise_error(
        SearchTermNotAllowedError,
        'Search term not allowed. See available searchable fields.'
      )

      expect do
        raise IdAttributeMissingError
      end.to raise_error(
        IdAttributeMissingError,
        'Data could not be indexed. Missing Id attribute'
      )

      expect do
        raise UndefinedDataSourceError
      end.to raise_error(
        UndefinedDataSourceError,
        'Undefined data source selected.'
      )

      expect do
        raise MissingDataSourceError
      end.to raise_error(
        MissingDataSourceError,
        'Missing data source selected.'
      )
    end
  end
end
