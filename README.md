# Zendesk Index

![Search Screenshot](https://bitbucket.org/blazejhadzik/zendesk_index/raw/master/screenshot.png)

This application allows you to search Users, Organizations, and Tickets.

* You can search using any field that is available.
* Searching using non-existing fields will result in a warning.
* Search allows to search empty fields, too.

## Configuration

Make sure you do the following:

* Have **Ruby 2.5.1** installed
* Run `./bin/setup`

## How to use

* Run `./bin/start`
* Use **arrow** keys to navigate and interact with an application.

## Input files

Input files (`users.json`, `organizations.json`, and `tickets.json`) can be placed under `lib/zendesk/data` directory. It is required that each record defines `_id` field used for indexing, otherwise the exception will be thrown.

## Tests
The application uses [Rspec](http://rspec.info/) framework. Additionally, [simplecov](https://github.com/colszowka/simplecov) has been used to report test coverage. In order to run tests simply run: `rspec spec/` from your app directory.

## Architecture

### The app contains 3 main components:

* /cluster (in-memory db and index)
* /records (represents single entity such as User, Ticket or Organization)
* /repositories (represents a collection of records)

### in-memory cluster (e.g. `Users`)

```ruby
  [1] pry(main)> user  = User.new(_id: 1, name: 'Blaze')
  [2] pry(main)> key   = name[:_id]
  [3] pry(main)> value = user
```


| Key             | Value         |
| --------------  |:-------------:|
| hashified `_id` | Ruby Object   |
| `_id` | Ruby Object   |
| `_id` | Ruby Object   |
| ...             | ...           |

### in-memory field index (e.g. `name`)

```ruby
  [1] pry(main)> name = "Blaze"
  => "Blaze"
  [2] pry(main)> key = name.hash
  => -3809235508409303293
```

| Key             | Value                           |
| --------------  |:-------------------------------:|
| hashified `key` | [Array] of in-memory primary keys (`_id`) |
| hashified `key` | [`_id`, ...] |
| hashified `key` | [`_id`, ...] |
| ...             | ...   |


It uses Ruby Hashes and `hash` method for improved performance as the keys are kept as Integers. 2 main objects are kept in memory:

```ruby
  [1] pry(main)> foo = "bar".hash
  => 829109532916987616
  [2] pry(main)> foo == "bar".hash
  => true
```
 
