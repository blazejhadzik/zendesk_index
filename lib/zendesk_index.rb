require 'json'
require 'zendesk_index/version'

require 'zendesk_index/search'
require 'zendesk_index/results_presenter'
require 'zendesk_index/custom_errors'
require 'zendesk_index/cluster/table'
require 'zendesk_index/cluster/index'

require 'zendesk_index/records/base_record'
require 'zendesk_index/records/user'
require 'zendesk_index/records/ticket'
require 'zendesk_index/records/organization'

require 'zendesk_index/repositories/acts_as_repository'
require 'zendesk_index/repositories/user_repository'
require 'zendesk_index/repositories/organization_repository'
require 'zendesk_index/repositories/ticket_repository'

module ZendeskIndex
  # Your code goes here...
end
