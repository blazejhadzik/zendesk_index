class ResultsPresenter
  def self.render_collection(collection = [])
    return puts 'No records found.' if collection.empty?

    collection.each do |item|
      item.to_h.map do |k, v|
        puts "%15.15s %s\n" % [k, v]
      end
      puts "\n"
    end
  end

  def self.render_list(collection = [])
    puts collection.map(&:to_s)
  end
end
