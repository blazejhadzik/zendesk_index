module Cluster
  class Index
    attr_reader :dictionary

    def initialize(indexable = nil)
      raise SearchTermNotAllowedError unless indexable

      @indexable = indexable.to_sym
      @dictionary = {}
    end

    def index(obj)
      raise IdAttributeMissingError unless obj[:_id]

      add(
        obj[@indexable],
        hashify(obj[:_id])
      )
    end

    def [](key)
      @dictionary.fetch(key, [])
    end

    protected

    def add(indexable_value, indexable_id)
      case indexable_value
      when Array
        deconstruct_arr(indexable_value, indexable_id)
      else
        key_hash = hashify(indexable_value.to_s)
        @dictionary[key_hash] ||= []
        @dictionary[key_hash] << indexable_id
      end
    end

    private

    def deconstruct_arr(indexable_value, indexable_id)
      indexable_value.each do |item|
        add(item, indexable_id)
      end
    end

    def hashify(value)
      value.hash
    end
  end
end
