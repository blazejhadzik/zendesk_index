module Cluster
  class Table
    def initialize(repository)
      @repository = repository
    end

    def index(search_attribute)
      @index_table = Index.new(search_attribute)

      @repository.all.each do |record|
        @index_table.index record
      end
    end

    def search(phrase)
      raise NoIndexTableFoundError unless @index_table

      find_by_ids(
        @index_table[hashify(phrase)]
      )
    end

    private

    def find_by_ids(ids)
      mem_cache.values_at(*ids)
    end

    def mem_cache
      @_storage ||= @repository.all.reduce({}) do |table, record|
        table.tap do |column|
          column[hashify(record[:_id])] = record
        end
      end
    end

    def hashify(value)
      value.hash
    end
  end
end
