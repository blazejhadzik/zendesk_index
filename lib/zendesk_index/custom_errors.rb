class NoIndexTableFoundError < StandardError
  def initialize
    super('Index has not been created yet. Create index before making query.')
  end
end

class SearchTermNotAllowedError < StandardError
  def initialize
    super('Search term not allowed. See available searchable fields.')
  end
end

class IdAttributeMissingError < StandardError
  def initialize
    super('Data could not be indexed. Missing Id attribute')
  end
end

class UndefinedDataSourceError < StandardError
  def initialize
    super('Undefined data source selected.')
  end
end

class MissingDataSourceError < StandardError
  def initialize
    super('Missing data source selected.')
  end
end
