class OrganizationRepository
  include ActsAsRepository

  def self.file_path
    File.expand_path('../../../data/organizations.json', __FILE__)
  end

  acts_as_repository(
    base_klass: Organization,
    source_path: file_path,
    symbolize_names: true
  )
end
