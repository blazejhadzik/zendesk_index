class TicketRepository
  include ActsAsRepository

  def self.file_path
    File.expand_path('../../../data/tickets.json', __FILE__)
  end

  acts_as_repository(
    base_klass: Ticket,
    source_path: file_path,
    symbolize_names: true
  )
end
