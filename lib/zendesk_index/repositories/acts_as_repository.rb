module ActsAsRepository
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def acts_as_repository(opts = {})
      include ActsAsRepository::InstanceMethods
      default_opts = {
        symbolize_names: true
      }

      attrs = default_opts.merge(opts)

      define_method(:base_klass) { attrs[:base_klass] }
      define_method(:source_path) { attrs[:source_path] }
      define_method(:symbolize_names) { attrs[:symbolize_names] }
    end

    def acts_as_repository?
      true
    end
  end

  module InstanceMethods
    def initialize
      connect!
    end

    def attributes
      base_klass::ATTR_LIST
    end

    def where(key:, value:)
      cluster.index(key)
      cluster.search(value)
    end

    def all
      @all
    end

    private

    def cluster
      @_cluster ||= Cluster::Table.new(self)
    end

    def connect!
      @all ||= JSON.parse(
        File.read(source_path),
        symbolize_names: symbolize_names
      ).flat_map do |raw|
        base_klass.new(raw)
      end
    rescue Errno::ENOENT
      raise MissingDataSourceError
    end
  end
end
