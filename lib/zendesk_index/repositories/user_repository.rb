class UserRepository
  include ActsAsRepository

  def self.file_path
    File.expand_path('../../../data/users.json', __FILE__)
  end

  acts_as_repository(
    base_klass: User,
    source_path: file_path,
    symbolize_names: true
  )
end
