class BaseRecord
  def initialize(attrs = {})
    self.class::ATTR_LIST.each do |attr_name|
      instance_variable_set("@#{attr_name}", attrs[attr_name])
    end
  end

  def [](attr_name)
    unless self.class::ATTR_LIST.include? attr_name
      raise SearchTermNotAllowedError
    end

    instance_variable_get("@#{attr_name}")
  end

  def to_h
    self.class::ATTR_LIST.reduce({}) do |hsh, attr_name|
      hsh[attr_name] = self[attr_name]
      hsh
    end
  end
end
