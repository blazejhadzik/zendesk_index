module ZendeskIndex
  class Search
    def query(repo_choice:, search_attr:, search_phrase:)
      db = available_repositories[repo_choice] || UndefinedDataSourceError

      db.where(
        key: search_attr.to_s,
        value: search_phrase.to_s
      )
    end

    def help(repo_choice:)
      db = available_repositories[repo_choice] || UndefinedDataSourceError

      db.base_klass::ATTR_LIST
    end

    private

    def available_repositories
      @_available_repositories ||= {
        'Users' => UserRepository.new,
        'Tickets' => TicketRepository.new,
        'Organizations' => OrganizationRepository.new
      }
    end
  end
end
